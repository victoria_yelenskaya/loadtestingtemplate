﻿README for !Template!_Load_Testing

1. !DraftJmeterScript — here you may found TemplateScript.jmx with usefull controllers and samplers examples.

2. FileSamples — here are examples of different file formats your may need to load on server during users scenario.

3. "5.0_ApacheJMeter.jar - Shortcut" - just shortcut to launch Jmeter, but your need to set correct address to ApacheJMeter.jar file on your machine.

4. To use ".cmd" scripts:
- GenerateHTMLReport.cmd
- LaunchJmeterViaFiddler.cmd
- RunJmeterScriptViaFiddler.cmd
open each script in any txt-editor and set correct address of your "JmeterBinDir".

5. Use GenerateProjectFolders.cmd to generate folder structure for a new project