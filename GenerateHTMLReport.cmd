SET JmeterBinDir=D:\Shchur_Evgen\Jmeter\apache-jmeter-5.0\bin
SET ReportSourceDir=%cd%\logs
SET ReportTargetDir=%cd%\HTML_Reports_%DATE%%Hour%_%TIME:~0,2%_%TIME:~3,2%

REM Check if reports dir already exist to remove it before new report generation
IF EXIST %ReportTargetDir%\NUL RMDIR /S /Q %ReportTargetDir%
mkdir %ReportTargetDir%

REM Create cmd to open last report in browser after generation
(
echo IF NOT EXIST "%ReportTargetDir%\index.html" TIMEOUT /T 60
echo start chrome %ReportTargetDir%\index.html
)>"OpenLastHTMLReport.cmd"
start OpenLastHTMLReport.cmd

REM generate HTML report from results.csv
cd %JmeterBinDir%
jmeter -g %ReportSourceDir%\results.csv -o %ReportTargetDir%